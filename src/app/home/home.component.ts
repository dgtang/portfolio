import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  skillsOpen: boolean = true; 
  portfolioOpen: boolean = false; 
  aboutOpen: boolean = false; 
  contactOpen: boolean = false; 

  constructor() { }

  ngOnInit(): void {
    
          
  }

  clickTab(tabName: any) {
      if (tabName == 'skills') {
       this.skillsOpen = true
       this.portfolioOpen = false
       this.aboutOpen = false
       this.contactOpen = false
      }
      if (tabName == 'portfolio') {
        this.skillsOpen = false
       this.portfolioOpen = true
       this.aboutOpen = false
       this.contactOpen = false
      }
      if (tabName == 'about') {
        this.skillsOpen = false
       this.portfolioOpen = false
       this.aboutOpen = true
       this.contactOpen = false
      }
      if (tabName == 'contact') {
        this.skillsOpen = false
       this.portfolioOpen = false
       this.aboutOpen = false
       this.contactOpen = true
      }
  }

}

